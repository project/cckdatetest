<?php

$content['type']  = array (
  'name' => 'Signup untimed',
  'type' => 'signup_untimed',
  'description' => 'Nodes without any date information at all.',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => true,
    'revision' => true,
    'sticky' => false,
  ),
  'old_type' => 'signup_untimed',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'signup_node_default_state' => 'enabled_on',
  'comment' => '2',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
