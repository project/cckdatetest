<?php

$content['type']  = array (
  'name' => 'Datestamp TZ Date',
  'type' => 'datestamp_tz_date',
  'description' => 'Nodes with CCK datestamp fields configured with TZ handling: Date\'s timezone.',
  'title_label' => 'Title',
  'body_label' => 'Body',
  'min_word_count' => '0',
  'help' => '',
  'node_options' => 
  array (
    'status' => true,
    'promote' => true,
    'sticky' => false,
    'revision' => false,
  ),
  'language_content_type' => '0',
  'old_type' => 'datestamp_tz_date',
  'orig_type' => '',
  'module' => 'node',
  'custom' => '1',
  'modified' => '1',
  'locked' => '0',
  'signup_node_default_state' => 'enabled_on',
  'comment' => '2',
  'comment_default_mode' => '4',
  'comment_default_order' => '1',
  'comment_default_per_page' => '50',
  'comment_controls' => '3',
  'comment_anonymous' => 0,
  'comment_subject_field' => '1',
  'comment_preview' => '1',
  'comment_form_location' => '0',
);
$content['fields']  = array (
  0 => 
  array (
    'label' => 'Datestamp TZ Date',
    'field_name' => 'field_datestamp_tz_date',
    'type' => 'datestamp',
    'widget_type' => 'date_text',
    'change' => 'Change basic information',
    'weight' => '-1',
    'default_value' => 'now',
    'default_value_code' => '',
    'default_value2' => 'same',
    'default_value_code2' => '',
    'input_format' => 'Y-m-d H:i:s',
    'input_format_custom' => '',
    'advanced' => 
    array (
      'label_position' => 'above',
      'text_parts' => 
      array (
        'year' => 0,
        'month' => 0,
        'day' => 0,
        'hour' => 0,
        'minute' => 0,
        'second' => 0,
      ),
    ),
    'increment' => 1,
    'year_range' => '-1:+3',
    'label_position' => 'above',
    'text_parts' => 
    array (
    ),
    'description' => '',
    'required' => 1,
    'multiple' => '0',
    'repeat' => 0,
    'todate' => 'optional',
    'granularity' => 
    array (
      'year' => 'year',
      'month' => 'month',
      'day' => 'day',
      'hour' => 'hour',
      'minute' => 'minute',
    ),
    'output_format_date' => 'Y-m-d H:i e',
    'output_format_custom' => '',
    'output_format_date_long' => 'm/d/Y',
    'output_format_custom_long' => '',
    'output_format_date_medium' => 'm/d/Y',
    'output_format_custom_medium' => '',
    'output_format_date_short' => 'm/d/Y',
    'output_format_custom_short' => '',
    'tz_handling' => 'date',
    'timezone_db' => 'UTC',
    'op' => 'Save field settings',
    'module' => 'date',
    'widget_module' => 'date',
    'columns' => 
    array (
      'value' => 
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => true,
      ),
      'value2' => 
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => true,
      ),
      'timezone' => 
      array (
        'type' => 'varchar',
        'length' => 50,
        'not null' => false,
        'sortable' => true,
      ),
      'offset' => 
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => true,
      ),
      'offset2' => 
      array (
        'type' => 'int',
        'not null' => false,
        'sortable' => true,
      ),
    ),
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
      ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
  ),
);
$content['extra']  = array (
  'title' => '-5',
  'body_field' => '0',
  'menu' => '-2',
);

