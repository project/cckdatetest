
This installation profile configures a test site that makes it easy to
test functionality with CCK date fields of all kinds and timezones.

For instructions on using an installation profile, see:
http://drupal.org/node/306267


------------------
REQUIREMENTS
------------------

* D6 core
* Install Profile API (http://drupal.org/project/install_profile_api)
* Content, Content Copy (http://drupal.org/project/cck)
* Date, Date API, Date Timezone (http://drupal.org/project/date)
* Signup (http://drupal.org/project/signup)
* Devel (http://drupal.org/project/devel)

Optionally, the profile will enable and configure calendars for each
date-enabled node type (see the 'CCKDATETEST_ENABLE_CALENDAR' constant
at the top of cckdatetest.profile).  This requires the following
additional modules:

* Views, Views UI (http://drupal.org/project/views)
* Calendar, Calendar iCal (http://drupal.org/project/calendar)


------------------
TASKS PERFORMED
------------------

0) Enables the required modules, installs the DB, etc.  Note that if
   you're running on a PHP4 site, the date_php4 module will also be
   enabled at this step.

1) Creates different node types for each possible configuration of CCK
   date fields.  For each field type (Date, Datestamp, or Datetime), 5
   content types are created for each possible kind of timezone
   handling (Date, Site, User, UTC, None) for a total of 15 node
   types.  Each type is named [datetype]_tz_[tztype], for example
   "datestamp_tz_site" or "date_tz_utc", etc.  These content types are
   all defined by .inc files in the content_types directory.

2) Creates roles and configure permissions.  In addition to providing
   default permissions for anonymous and authenticated users, this
   creates 3 additional roles:
   * signupadmin -- Can do extra signup things to their own nodes.
   * nodeadmin -- Has 'administer nodes' to edit/create all nodes.
   * siteadmin -- Has broad site-wide powers over signups and nodes.
   Devel permissions are configured very broadly, to allow all users
   to switch among each other, execute PHP code, etc.

3) Generates a bunch of users named by the city they're from from a
   wide range of timezones across most continents.  For each regular
   user ("Oakland") an admin user is also created ("Oakland-admin")
   which also belongs to the 'signupadmin' role.  Additionally, a few
   site-wide admin users are created, one for each of the special
   administrative roles on the site.

4) Configures the other modules on the site, in particular devel and signup.

5) Generates 4 nodes for each date-enabled content type:
   * past: 2 days ago
   * now: the current time
   * near: 2 hours from now
   * future: 2 days from now
   Nodes that use the site-wide timezones use America/New_York.
   Nodes that use per-node timezones use America/Chicago.
   Nodes that use the user's timezone use America/Los_Angeles.

6) [Optional] Enables and configures a separate calendar view for each
   date-enabled node type.  If 'CCKDATETEST_ENABLE_CALENDAR' is TRUE
   (which it is by default), these calendar views can be found on the
   new site at the following paths:
   /calendar-field-[date_type]-tz-[tz_handling]
   For example:
   /calendar-field-date-tz-utc
   /calendar-field-datestamp-tz-site
   ...


------------------
CONFIGURATION
------------------

There are a handful of constants at the top of cckdatetest.profile
that can be used to customize the resulting site.  If you don't change
these at all, you'll get a working site.  All of the generated users
will use "example.com" as the domain for their e-mail addresses.  The
site timezone will be set to America/New_York, and the user that
creates all the nodes will be the "Oakland-admin" user.

Also note that all of the content types found in the "content_types"
directory will be created by default.  If you only want to create and
test a subset of content types, you should move the .inc files you
don't want out of the "content_types" directory.  In the future, it's
possible the install profile will provide a page of checkboxes to
select which content types to create and generate nodes for.


------------------
CREDITS
------------------

Written by Derek Wright "dww" http://drupal.org/user/46549

