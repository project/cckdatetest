<?php

/**
 * Should the profile enable and configure calendar?
 */
define('CCKDATETEST_ENABLE_CALENDAR', TRUE);

/**
 * Password for all of the auto-generated users.
 */
define('CCKDATETEST_DEFAULT_PASSWORD', 'a');

/**
 * Domain used for the e-mail addresses of all auto-generated users.
 */
define('CCKDATETEST_DEFAULT_DOMAIN', 'example.com');

/**
 * The CCK date site-wide timezone to use for this test site.
 */
define('CCKDATETEST_SITE_TZ', 'America/New_York');

/**
 * User for all pre-generated nodes on the site.
 *
 * @see cckdatetest_create_users()
 */
define('CCKDATETEST_DEFAULT_USER', 'Oakland-admin');

/**
 * Default username for the admin user on the site (uid 1).
 *
 * This value can be changed during the installation wizard so you
 * should only change this constant if you regularly want a different
 * username to appear on that form by default.
 */
define('CCKDATETEST_DEFAULT_ADMIN_USER', 'root');


/**
 * Return an array of the modules to be enabled when this profile is installed.
 *
 * @return
 *   An array of modules to enable.
 */
function cckdatetest_profile_modules() {
  $modules = array(
    'help', 'menu', 'taxonomy', 'dblog',
    'comment', 'help', 'menu', 'taxonomy', 'dblog',
    'install_profile_api',
    'content', 'content_copy', 'date_timezone', 'date_api', 'date',
    'signup', 'devel',
    'views', 'views_ui',
  );
  if (version_compare(PHP_VERSION, '5.2', '<')) {
    $modules[] = 'date_php4';
  }
  if (CCKDATETEST_ENABLE_CALENDAR) {
    $modules += array('calendar', 'calendar_ical');
  }
  return $modules;
}

/**
 * Return a description of the profile for the initial installation screen.
 *
 * @return
 *   An array with keys 'name' and 'description' describing this profile,
 *   and optional 'language' to override the language selection for
 *   language-specific profiles.
 */
function cckdatetest_profile_details() {
  return array(
    'name' => 'CCK date field test site',
    'description' => 'Select this profile to create a test site for CCK date fields.'
  );
}

/**
 * Return a list of tasks that this profile supports.
 *
 * @return
 *   A keyed array of tasks the profile will perform during
 *   the final stage. The keys of the array will be used internally,
 *   while the values will be displayed to the user in the installer
 *   task list.
 */
function cckdatetest_profile_task_list() {
}

/**
 * Perform any final installation tasks for this profile.
 *
 * @param $task
 *   The current $task of the install system. When hook_profile_tasks()
 *   is first called, this is 'profile'.
 * @param $url
 *   Complete URL to be used for a link or form action on a custom page,
 *   if providing any, to allow the user to proceed with the installation.
 *
 * @return
 *   An optional HTML string to display to the user. Only used if you
 *   modify the $task, otherwise discarded.
 */
function cckdatetest_profile_tasks(&$task, $url) {
  // Initialize install_profile_api.
  $core_required = array('block', 'filter', 'node', 'system', 'user');
  install_include(array_merge(cckdatetest_profile_modules(), $core_required));

  cckdatetest_create_roles();

  cckdatetest_create_users();

  cckdatetest_configure_date();

  cckdatetest_create_content_types();

  cckdatetest_configure_devel();

  cckdatetest_configure_signup();

  cckdatetest_create_nodes();

  // Update the menu router information.
  menu_rebuild();
}

/** 
 * Load all the content type definitions in content_types/*.inc.
 */
function cckdatetest_create_content_types() {
  $path = drupal_get_path('profile', 'cckdatetest') .'/content_types';
  $calendar_options = array();
  if ($handle = opendir($path)) {
    while ($file = readdir($handle)) {
      if (preg_match('@(.*)\.inc$@', $file)) {
        $file_path = $path .'/'. $file;
        install_content_copy_import_from_file($file_path);

        // Add default calendars for all the date content types if desired.
        if (CCKDATETEST_ENABLE_CALENDAR && substr($file, 0, 4) == 'date') {
          $type_name = str_replace('.inc', '', $file);
          $field_name = 'field_'. $type_name;
          $option = array(
            'name' => 'calendar_'. $field_name,
            'description' => 'An event calendar for the '. $field_name .' field.',
            'path' => 'calendar-'. str_replace('_', '-', $field_name),
            'types' => array($type_name),
            'date_fields' => array($field_name),
          );
          $calendar_options[] = $option;
        }
      }
    }
    if (!empty($calendar_options)) {
      variable_set('calendar_default_view_options', $calendar_options);
    }
  }
}

/**
 * Create roles and configure permissions for each one.
 *
 * In addition to providing default permissions for anonymous and
 * authenticated users, this creates 3 additional roles:
 * - signupadmin -- Can do extra signup things to their own nodes.
 * - nodeadmin -- Has 'administer nodes' to edit/create all nodes.
 * - siteadmin -- Has broad site-wide powers over signups and nodes.
 *
 * Devel permissions are configured very broadly, to allow all users
 * to switch among each other, execute PHP code, etc.
 */
function cckdatetest_create_roles() {
  // Initialize some roles.
  $roles = array(
    'signupadmin' => array(
      'administer signups for own content',
      'email users signed up for own content',
      'cancel own signups',
    ),
    'nodeadmin' => array(
      'administer nodes',
    ),
    'siteadmin' => array(
      'administer nodes',
      'administer content types',
      'administer all signups',
      'email all signed up users',
      'view all signups',
      'access administration pages',
      'access site reports',
      'administer site configuration',
    ),
  );
  foreach ($roles as $role => $permissions) {
    $rid = install_add_role($role);
    install_add_permissions($rid, $permissions);
  }

  // Perms for the fixed permissions
  $anon_permissions = array(
    'access devel information',
    'sign up for content',
  );
  install_add_permissions(DRUPAL_ANONYMOUS_RID, $anon_permissions);

  $auth_permissions = array(
    'switch users',
    'access user profiles',
    'execute php code',
  );
  $auth_permissions = array_merge($anon_permissions, $auth_permissions);

  foreach (node_get_types() as $type => $info) {
    $auth_permissions[] = "create $type content";
    $auth_permissions[] = "edit own $type content";
  }
  install_add_permissions(DRUPAL_AUTHENTICATED_RID, $auth_permissions);
}

/**
 * Creates users for the test site from numerous timezones around the world.
 * 
 * This generates a bunch of users named by the city they're from,
 * from a wide range of timezones across most continents.  For each
 * regular user (e.g. "Oakland") an admin user is also created
 * (e.g. "Oakland-admin") which also belongs to the 'signupadmin' role.
 *
 * Additionally, a few site-wide admin users are created, one for each
 * of the special administrative roles on the site.
 *
 * @see cckdatetest_create_roles()
 * @see cckdatetest_add_user()
 */
function cckdatetest_create_users() {
  $roles = array_flip(user_roles());
  $users = array(
    'nodeadmin' => array(
      'roles' => array($roles['nodeadmin'] => 'nodeadmin'),
      'timezone_name' => 'UTC',
    ),
    'siteadmin' => array(
      'roles' => array($roles['siteadmin'] => 'siteadmin'),
      'timezone_name' => 'UTC',
    ),
    'Oakland' => array(
      'timezone_name' => 'America/Los_Angeles',
    ),
    'Chicago' => array(
      'timezone_name' => 'America/Chicago',
    ),
    'NY' => array(
      'timezone_name' => 'America/New_York',
    ),
    'Sao Paulo' => array(
      'timezone_name' => 'America/Sao_Paulo',
    ),
    'Cairo' => array(
      'timezone_name' => 'Africa/Cairo',
    ),
    'Tokyo' => array(
      'timezone_name' => 'Asia/Tokyo',
    ),
  );
  foreach ($users as $name => $info) {
    cckdatetest_add_user($name, $info);
    if (!preg_match('/admin/', $name)) {
      $info['roles'][$roles['signupadmin']] = 'signupadmin';
      cckdatetest_add_user("$name-admin", $info);
    }
  }
}

/**
 * Add an auto-generated user.
 *
 * @param $name
 *   The username (also used to generate the e-mail address).
 * @param $info
 *   Array of other information about the user (timezone, roles, etc).
 *
 * @see cckdatetest_create_users()
 */
function cckdatetest_add_user($name, $info) {
  $default_info = array(
    'status' => 1,
    'pass' => CCKDATETEST_DEFAULT_PASSWORD,
  );
  $user_data = array_merge($info, $default_info);
  $user_data['name'] = $name;
  $user_data['mail'] = $name .'@'. CCKDATETEST_DEFAULT_DOMAIN;
  user_save(new stdClass(), $user_data);
}

function cckdatetest_configure_date() {
  // Set the default site-wide TZ
  variable_set('date_default_timezone_name', CCKDATETEST_SITE_TZ);
}

function cckdatetest_configure_devel() {
  // Configure blocks.
  install_add_block('menu', 'devel', 'garland', 1, 1, 'left');
  install_add_block('devel', 0, 'garland', 1, 2, 'left');
  install_add_block('devel', 2, 'garland', 1, 0, 'footer');
  
  // Configure settings.
  // Save any old SMTP library
  $smtp_old = variable_get('smtp_library', '');
  $smtp_new = drupal_get_filename('module', 'devel');
  if ($smtp_old != '' && $smtp_old != $smtp_new) {
    variable_set('devel_old_smtp_library', $smtp_old);
  }
  variable_set('smtp_library', $smtp_new);
  // Make sure the switch user block is big enough for all the
  // auto-generated users on the site.
  variable_set('devel_switch_user_list_size', 15);
}

function cckdatetest_configure_signup() {
  // Site wide default signup node settings.
  $forward = 'signup-notify@'. CCKDATETEST_DEFAULT_DOMAIN;
  $tokens = <<<END
| node_title: %node_title
| node_url: %node_url
| node_start_time: %node_start_time
| user_name: %user_name
| user_mail: %user_mail
| user_signup_info: %user_signup_info
END;
  $confirmation = "Confirmation:\n". $tokens;
  $reminder = "Reminder:\n". $tokens;
  db_query("UPDATE {signup} SET forwarding_email = '%s', confirmation_email = '%s', reminder_email = '%s' WHERE nid = %d", $forward, $confirmation, $reminder, 0);

  // Site wide configuration settings.
  variable_set('signup_fieldset_collapsed', 0);

  // We need to select the signup_date_field for each node type, since we
  // can't seem to specify that correctly via the include files used for
  // install_content_copy_import_from_file().
  foreach (node_get_types() as $type => $info) {
    if (preg_match('/date/', $type)) {
      variable_set("signup_date_field_$type", "field_$type");
    }
    else {
      variable_set("signup_date_field_$type", 'none');
    }
  }
}

/**
 * Generate nodes for each content type that was defined.
 *
 * This loops over all of the content types that were created.  If it
 * is a date-enabled content type, 4 nodes are generated: past, now,
 * near, and future.  All nodes are created owned by the user
 * specified in CCKDATETEST_DEFAULT_USER.
 * 
 * @see cckdatetest_date_field()
 * @see cckdatetest_save_node()
 */
function cckdatetest_create_nodes() {
  module_load_include('inc', 'node', 'node.pages');
  $uid = db_result(db_query("SELECT uid FROM {users} WHERE name = '%s'", CCKDATETEST_DEFAULT_USER));
  foreach (node_get_types() as $type => $info) {
    if (preg_match('/date/', $type)) {
      $field_name = 'field_'. $type;
      foreach (array('past', 'now', 'near', 'future') as $when) {
        unset($node);
        $node = new stdClass();
        $node->type = $type;
        $node->uid = $uid;
        $node->title = "$type $when";
        $node->$field_name = cckdatetest_date_field($type, $when);
        cckdatetest_save_node($node);
      }
    }
    else {
      unset($node);
      $node = new stdClass();
      $node->type = $type;
      $node->uid = $uid;
      $node->title = $type;
      cckdatetest_save_node($node);
    }
  }
}

/**
 * Helper function to handle shared attributes and to save each node.
 */
function cckdatetest_save_node($node) {
  $node->body = "node ($node->type)";
  $node->teaser = node_teaser($node->body);
  $node->filter = variable_get('filter_default_format', 1);
  $node->format = FILTER_FORMAT_DEFAULT;
  $node->language = '';
  $node->revision = 1;
  $node->promote = 1;
  node_save($node);
}

/**
 * Generate the proper elements for a given CCK date field.
 *
 * @param $type
 *   Name of the CCK date field type (e.g. "datestamp_tz_site").
 * @param $when
 *   String describing when in time we want the date values.  Can be:
 *   'past': 2 days ago
 *   'now': right now
 *   'near': 2 hours from now
 *   'future': 2 days from now
 *
 * @return
 *   A properly formatted array of values for the start and to dates
 *   appropriate for this field type. 
 *
 * @see cckdatetest_create_nodes()
 */
function cckdatetest_date_field($type, $when) {
  $parts = split('_', $type);
  $field_date_type = $parts[0];
  $field_tz_type = $parts[2];
  switch ($field_tz_type) {
    case 'utc':
      $tz = 'UTC';
      break;

    case 'site':
    case 'none':
      $tz = date_default_timezone_name();
      break;

    case 'date':
      $tz = 'America/Chicago';
      break;

    case 'user':
      $tz = 'America/Los_Angeles';
      break;
  }

  $date = date_now($tz);
  switch ($when) {
    case 'past':
      date_modify($date, '-2 days');
      break;

    case 'near':
      date_modify($date, '+2 hours');
      break;

    case 'future':
      date_modify($date, '+2 days');
      break;
  }

  $field = array();
  if ($field_tz_type == 'date') {
    // If this date has a date-specific tz, we need to compute the offset
    // before we set the $date object to print UTC.
    $field[0]['offset'] = date_offset_get($date);
    $field[0]['offset2'] = date_offset_get($date);
  }
  if ($field_tz_type != 'none') {
    // If there's any TZ handling at all, store the local TZ for this date.
    $field[0]['timezone'] = $tz;
    // Make sure the date object is going to print UTC values.
    date_timezone_set($date, timezone_open('UTC'));
  }

  $date_format = $field_date_type == 'datestamp' ? 'U' : 'Y-m-d H:i';
  $field[0]['value'] = date_format($date, $date_format);
  date_modify($date, '+2 hours');  
  $field[0]['value2'] = date_format($date, $date_format);

  return $field;
}

/**
 * Implementation of hook_form_alter().
 *
 * Allows the profile to alter the site-configuration form. This is
 * called through custom invocation, so $form_state is not populated.
 */
function cckdatetest_form_alter(&$form, $form_state, $form_id) {
  if ($form_id == 'install_configure') {
    // Set default for site name field.
    $form['site_information']['site_name']['#default_value'] = t('D6 CCK date testsite');
    $form['site_information']['site_mail']['#default_value'] = 'd6-cckdatetest@'. CCKDATETEST_DEFAULT_DOMAIN;
    $form['admin_account']['account']['name']['#default_value'] = CCKDATETEST_DEFAULT_ADMIN_USER;
    $form['admin_account']['account']['mail']['#default_value'] = CCKDATETEST_DEFAULT_ADMIN_USER .'@'. CCKDATETEST_DEFAULT_DOMAIN;
  }
}

